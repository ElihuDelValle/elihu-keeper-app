import React, { useContext } from "react";
import { StyleSheet, View } from "react-native";
import { NavigationEvents } from "react-navigation";
import { Context as AuthContext } from "../context/AuthContext";
import AuthForm from "../components/AuthForm";
import NavigationLink from "../components/NavigationLink";

const SignupScreen = () => {
  const { state, signup, clearErrorMessages } = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <NavigationEvents onWillFocus={clearErrorMessages} />
      <AuthForm
        headerText="Signup for Elihu-Keeper"
        buttonText="Sign Up"
        onSubmit={signup}
        errorMessage={state.errorMessage}
      />
      <NavigationLink
        text="Already have an account? Sign in here!"
        routeName="Signin"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginBottom: 150,
  },
});

SignupScreen.navigationOptions = () => {
  return {
    headerShown: false,
  };
};

export default SignupScreen;

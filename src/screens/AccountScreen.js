import React, { useContext } from "react";
import { StyleSheet } from "react-native";
import { Button, Text } from "react-native-elements";
import { SafeAreaView } from "react-navigation";
import Spacer from "../components/Spacer";
import { Context as AuthContext } from "../context/AuthContext";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const AccountScreen = () => {
  const { signout } = useContext(AuthContext);
  return (
    <SafeAreaView forceInset={{ top: "always" }}>
      <Spacer>
        <Text style={styles.default}> Sign out! </Text>
      </Spacer>
      <Spacer>
        <Button title="Sign out" onPress={signout} />
      </Spacer>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  default: {
    fontSize: 30,
    alignSelf: "center",
  },
});

AccountScreen.navigationOptions = {
  title: "Account Details",
  tabBarIcon: <MaterialCommunityIcons name="account-circle" size={20} />,
};

export default AccountScreen;

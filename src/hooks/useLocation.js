import { useState, useEffect } from "react";
import {
  requestPermissionsAsync,
  Accuracy,
  watchPositionAsync,
} from "expo-location";

export default (useGPS, callback) => {
  const [err, setErr] = useState(null);

  useEffect(() => {
    let subscriber;
    const startWatching = async () => {
      try {
        if ((await requestPermissionsAsync()).granted) {
          subscriber = await watchPositionAsync(
            {
              accuracy: Accuracy.BestForNavigation,
              timeInterval: 1000,
              distanceInterval: 5,
              mayShowUserSettingsDialog: true,
            },
            callback
          );
        } else {
          setErr("Rejected");
        }
      } catch (e) {
        console.log(e);
      }
    };

    if (useGPS) {
      startWatching();
    } else {
      if (subscriber) {
        subscriber.remove();
      }
      subscriber = null;
    }

    return () => {
      if (subscriber) {
        subscriber.remove();
      }
    };
  }, [useGPS, callback]);

  return [err];
};

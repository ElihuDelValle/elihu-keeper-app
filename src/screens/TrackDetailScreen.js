import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import { Text } from "react-native-elements";
import { Context as TrackContext } from "../context/TrackContext";
import MapView, { Polyline } from "react-native-maps";

const TrackDetailScreen = ({ navigation }) => {
  const { state } = useContext(TrackContext);
  const _id = navigation.getParam("_id");
  const track = state.find((item) => item._id === _id);
  const initialCoords = track.locations[0].coords;
  console.log(initialCoords);

  return (
    <View>
      <Text style={styles.default}>{track.name}</Text>
      <MapView
        initialRegion={{
          longitudeDelta: 0.01,
          latitudeDelta: 0.01,
          ...initialCoords,
        }}
        style={styles.map}
      >
        <Polyline coordinates={track.locations.map((item) => item.coords)} />
      </MapView>
    </View>
  );
};

const styles = StyleSheet.create({
  default: {
    fontSize: 30,
  },
  map: {
    height: 300,
  },
});

export default TrackDetailScreen;

import React, { useContext } from "react";
import { StyleSheet, View } from "react-native";
import { NavigationEvents } from "react-navigation";
import { Context as AuthContext } from "../context/AuthContext";
import AuthForm from "../components/AuthForm";
import NavigationLink from "../components/NavigationLink";

const SigninScreen = () => {
  const { state, signin, clearErrorMessages } = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <NavigationEvents onWillFocus={clearErrorMessages} />
      <AuthForm
        headerText="Login to your account"
        buttonText="Sign in"
        onSubmit={signin}
        errorMessage={state.errorMessage}
      />
      <NavigationLink
        text="Don't have an account? Sign up here!"
        routeName="Signup"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    marginBottom: 150,
  },
});

SigninScreen.navigationOptions = () => {
  return {
    headerShown: false,
  };
};

export default SigninScreen;

import createDataContext from "./createDataContext";
import trackerApi from "../api/trackerApi";
import { AsyncStorage } from "react-native";
import { navigate } from "../utils/navigationRef";

const authReducer = (state, action) => {
  switch (action.type) {
    case "authenticate":
      return { ...state, token: action.payload, errorMessage: "" };
    case "add_error":
      return { ...state, errorMessage: action.payload };
    case "clear_error_msg":
      return { ...state, errorMessage: "" };
    default:
      return state;
  }
};

const tryLocalSignin = (dispatch) => async () => {
  const token = await AsyncStorage.getItem("token");
  if (token) {
    dispatch({ type: "authenticate", payload: token });
    navigate("mainFlow");
  } else {
    navigate("loginFlow");
  }
};

const clearErrorMessages = (dispatch) => () => {
  dispatch({ type: "clear_error_msg" });
};

const signup = (dispatch) => async ({ email, password }) => {
  try {
    const response = await trackerApi.post("/signup", { email, password });
    await AsyncStorage.setItem("token", response.data.token);
    dispatch({ type: "authenticate", payload: response.data.token });
    navigate("mainFlow");
  } catch (err) {
    dispatch({
      type: "add_error",
      payload: "Something went wrong with sign up",
    });
  }
};

const signin = (dispatch) => async ({ email, password }) => {
  try {
    const response = await trackerApi.post("/signin", { email, password });
    await AsyncStorage.setItem("token", response.data.token);
    dispatch({ type: "authenticate", payload: response.data.token });
    navigate("mainFlow");
  } catch (err) {
    dispatch({
      type: "add_error",
      payload: "Login failed",
    });
  }
};

const signout = (dispatch) => async () => {
  await AsyncStorage.removeItem("token");
  dispatch({ type: "authenticate", payload: null });
  navigate("loginFlow");
};

export const { Provider, Context } = createDataContext(
  authReducer,
  { signup, signin, signout, clearErrorMessages, tryLocalSignin },
  { token: null, errorMessage: "" }
);

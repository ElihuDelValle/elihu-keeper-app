import React, { useContext } from "react";
import { Input, Button } from "react-native-elements";
import Spacer from "./Spacer";
import { Context as LocationContext } from "../context/LocationContext";
import useSaveTrack from "../hooks/useSaveTrack";

const TrackForm = () => {
  const {
    state: { name, recording, locations },
    startRecording,
    stopRecording,
    changeName,
  } = useContext(LocationContext);

  const [saveTrack] = useSaveTrack();

  return (
    <>
      <Spacer>
        <Input
          placeholder="Enter training name"
          value={name}
          onChangeText={changeName}
        />
      </Spacer>
      <Spacer>
        {recording ? (
          <Button
            title="Stop Training"
            onPress={stopRecording}
            buttonStyle={{ backgroundColor: "red" }}
          />
        ) : (
          <Button
            title="Start Training!"
            onPress={startRecording}
            buttonStyle={{ backgroundColor: "green" }}
          />
        )}
      </Spacer>
      {!recording && locations.length ? (
        <Spacer>
          <Button
            title="Save Training"
            onPress={saveTrack}
            buttonStyle={{ backgroundColor: "blue" }}
          />
        </Spacer>
      ) : null}
    </>
  );
};

export default TrackForm;

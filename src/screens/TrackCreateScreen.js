//import "../utils/mockLocation";
import React, { useContext, useCallback } from "react";
import { Text } from "react-native-elements";
import { SafeAreaView, withNavigationFocus } from "react-navigation";
import Map from "../components/Map";
import { Context as LocationContext } from "../context/LocationContext";
import useLocation from "../hooks/useLocation";
import TrackForm from "../components/TrackForm";
import { MaterialCommunityIcons } from "@expo/vector-icons";

const TrackCreateScreen = ({ isFocused }) => {
  const {
    state: { recording },
    addLocation,
  } = useContext(LocationContext);

  const myCallback = useCallback(
    (location) => {
      addLocation(location, recording);
    },
    [recording]
  );

  const [err] = useLocation(isFocused || recording, myCallback);

  return (
    <SafeAreaView forceInset={{ top: "always" }}>
      <Text h2> Start Session</Text>
      <Map />
      {err ? <Text> Permission to access location is required!</Text> : null}
      <TrackForm />
    </SafeAreaView>
  );
};

TrackCreateScreen.navigationOptions = {
  title: "Start Training",
  tabBarIcon: <MaterialCommunityIcons name='run' size={20}/>
};

export default withNavigationFocus(TrackCreateScreen);
